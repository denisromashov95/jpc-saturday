package week2.genericscollections.task9;

import java.util.ArrayList;
import java.util.Iterator;

/*
Удалить элемент из списка, если он нечетный.
 */
public class DeleteListElement {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);

        //ConcurrentModificationException
//        for (Integer element : list) {
//            if (element % 2 != 0) {
//                list.remove(element);
//            }
//        }

        //1 способ
//        list.removeIf(integer -> integer % 2 != 0);
//        System.out.println(list);

        //2 способ - создать копию списка и работать с ним
        //3 способ - iterator
        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()) {
            int element = iterator.next();
            if (iterator.next() % 2 != 0) {
                iterator.remove();
            }
        }

        System.out.println(list);

    }
}
