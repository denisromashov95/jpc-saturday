package week2.genericscollections.task4;

import java.util.Collections;
import java.util.List;

/*
Реализовать метод, который считает количество содержащихся элементов в переданном List
(Входные параметры: List, element - кол-во которых необходимо посчитать)
 */
public class ListUtils {
    private ListUtils() {
    }

    //Collections.frequency();
    public static <T> int countIf(List<T> elements, T element) {
        int counter = 0;
        for (T elem : elements) {
            //Сравнение по значению
//            if (elem.equals(element)) {
//                counter++;
//            }

            //Сравнение по ССЫЛКИ!
            if (elem == element) {
                counter++;
            }
        }
        return counter;
    }

}
