package week2.genericscollections.task1;

import java.math.BigInteger;

/*
Создать класс Pair, который умеет хранить два значения:
1) Одинакового типа
2) Любого типа (K, V)
3) число и строка только
 */
public class Main {
    public static void main(String[] args) {
//        Pair<Boolean> pair1 = new Pair<>();
//        pair1.key = true;
//        pair1.value = false;
//
//        Pair<Integer> pair2 = new Pair<>();
//        pair2.key = 1;
//        pair2.value = 99;
//
//        pair1.print();
//        pair2.print();

//        Pair<Boolean, String> pair = new Pair<>();
//        pair.key = true;
//        pair.value = "Hello!";
//        pair.print();

//        Pair<Number, String> pair = new Pair<>();
//        pair.key = 10;
//        pair.value = "Десять";
//        pair.print();
//
//        pair.key = new BigInteger("123");
//        pair.value = true;

        Pair<Integer, String> p1 = new Pair<>(1, "apple");
        Pair<Integer, String> p2 = new Pair<>();
        Pair<Integer, String> p3 = new Pair<>(1, "apple");
        p2.setKey(2);
        p2.setValue("pear");

        System.out.println(Pair.compare(p1, p2));
        System.out.println(Pair.compare(p1, p3));
    }
}
