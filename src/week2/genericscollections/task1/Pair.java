package week2.genericscollections.task1;

public class Pair<K extends Number, V extends String> {
    private K key;
    private V value;

    public Pair() {
    }

    public Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    public static <K extends Number, V extends String> boolean compare(Pair<K, V> pair1, Pair<K, V> pair2) {
        return pair1.getKey().equals(pair2.getKey())
                && pair1.getValue().equals(pair2.getValue());
    }

    public void print() {
        System.out.println("Key: " + key + " Value: " + value);
    }
}
