package week4.streams.task2;

/*
Проверить, является ли текст палиндром.
Из исходной строки с помощью стримов убрать все символы,
не являющиеся цифрой или буквой, привести к нижнему регистру.
 */
public class Palindrome {
    public static void main(String[] args) {
        System.out.println(isPalindrome(",./aB/1.21/bA.,;"));

    }

    //                .filter(c -> Character.isLetter(c) || Character.isDigit(c))
//                .filter(Character::isLetter)
//                .filter(Character::isDigit)

    public static boolean isPalindrome(String initialString) {
        StringBuilder leftToRight = new StringBuilder();

        initialString.chars()
                .filter(Character::isLetterOrDigit)
                .map(Character::toLowerCase)
                .forEach(leftToRight::appendCodePoint);
        System.out.println(leftToRight);
        StringBuilder rightToLeft = new StringBuilder(leftToRight).reverse();

        return leftToRight.toString().equals(rightToLeft.toString());
    }

}
