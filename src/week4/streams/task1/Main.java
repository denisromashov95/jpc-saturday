package week4.streams.task1;

import week4.functional.task2.Square;

import java.util.List;

/*
Использовать реализованный функциональный интерфейс Square (Task2) на списке чисел, вывести на экран.
 */
public class Main {
    public static void main(String[] args) {
        List<Integer> nums = List.of(3, 5, 10, 100, 120);
        Square square = x -> x * x;

        nums.stream()
//                .map(num -> square.calculateSquare(num))
                .map(square::calculateSquare)
                .forEach(System.out::println);
    }
}
