package week4.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/*
Создать папку и подпаку в src/
 */
public class Task2 {
    public static void main(String[] args) {
        Path path1 = Paths.get("src/newPackageFolder");
        Path path2 = Paths.get("src/newPackageFolder/childSubDirectory");

        try {
            Files.createDirectory(path1);
            Files.createDirectory(path2);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}
