package week4.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/*
Переместить файл в папку
 */
public class Task4 {
    public static void main(String[] args) {
        Path soucePath = Paths.get("src/test_copy.txt");
        Path targetPath = Paths.get("src/newPackageFolder/test_copy.txt");
        try {
            Files.move(soucePath, targetPath, StandardCopyOption.REPLACE_EXISTING);
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
