package week4.functional.task2;

/*
С помощью функционального интерфейса выполнить подсчет квадрата числа
 */
public class Main {
    
    public static void main(String[] args) {
        Square square = new Square() {
            @Override
            public int calculateSquare(int x) {
                return x * x;
            }
        };
        System.out.println(square.calculateSquare(8));

        Square sl = x -> x * x;
        System.out.println(sl.calculateSquare(5));

    }
}
