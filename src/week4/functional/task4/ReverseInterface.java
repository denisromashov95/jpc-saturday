package week4.functional.task4;

@FunctionalInterface
public interface ReverseInterface {
    String getReverseString(String initialString);
}
