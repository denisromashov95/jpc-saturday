
1. https://habr.com/ru/post/512730/ - Lambda-выражения в Java
1. https://annimon.com/article/2778 - Полное руководство по Java 8 Stream API в картинках и примерах
1. https://habr.com/ru/post/337350/ - Используйте Stream API проще
1. https://habr.com/ru/post/693666/ - Stream API
1. https://habr.com/ru/post/437038/ - Stream API by Benjamin Winterberg
1. https://habr.com/ru/post/677610/ - Функциональные интерфейсы в Java 8 → Consumer, Supplier, Predicate и Function.
1. https://habr.com/ru/post/235585/ - NIO vs IO
1. https://www.tune-it.ru/web/ivanuskov/blog/-/blogs/java-nio - NIO - what is it?
1. https://www.baeldung.com/java-8-streams - Stream API
1. https://webhamster.ru/mytetrashare/index/mtb426/1578962094eafr3wruxs - Шпаргалка по Stream API
1. https://itnan.ru/post.php?c=1&p=270383 - Шпаргалка по Stream API
1. https://highload.today/java-stream-api/ - Шпаргалка по Stream API (нужен VPN)
1. https://www.youtube.com/watch?v=O8oN4KSZEXE, https://www.youtube.com/watch?v=i0Jr2l3jrDA - Сергей Куксенко —   API
1. https://metanit.com/java/tutorial/10.9.php - параллельные стримы
1. https://www.baeldung.com/java-when-to-use-parallel-stream - When to Use a Parallel Stream in Java
