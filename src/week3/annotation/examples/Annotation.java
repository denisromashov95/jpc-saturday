package week3.annotation.examples;

import java.util.HashSet;
import java.util.Set;

public class Annotation {
    public static void main(String[] args) {
        foo();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public static void suppressSet() {
        Set set = new HashSet<>();
        set.add(1);
        System.out.println(set.size());
    }

    @Deprecated
    public static void foo() {}
}
