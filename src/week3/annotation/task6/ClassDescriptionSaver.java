package week3.annotation.task6;

import week3.annotation.task5.ClassDescription;
import week3.annotation.task5.PerfectClass;

public class ClassDescriptionSaver {
    public static void writeDescription(Class<?> clazz) {
        if (!clazz.isAnnotationPresent(ClassDescription.class)) {
            return;
        }

        ClassDescription classDescription = clazz.getAnnotation(ClassDescription.class);

        System.out.println("Автор: " + classDescription.author());
        System.out.println("Дата создания: " + classDescription.date());
        System.out.println("Текущая версия: " + classDescription.currentRevision());

        System.out.println("Список проверяющих: ");
        for (String reviewer : classDescription.reviewers()) {
            System.out.println("> " + reviewer);
        }
    }

    public static void main(String[] args) {
        writeDescription(PerfectClass.class);
    }
}
