package week3.reflection.task3;

import week3.reflection.task2.Task2;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/*
    Продолжение задачи 2. Создать экземпляр (инстанс) класса Task вывести значения его полей.
 */
public class Task3 {

    public static void printAllFieldsWithValues(Object instance) throws IllegalAccessException {
        for (Field field : instance.getClass().getDeclaredFields()) {
            int mods = field.getModifiers();
            if (Modifier.isPublic(mods)) System.out.print("public ");
            if (Modifier.isProtected(mods)) System.out.print("protected ");
            if (Modifier.isPrivate(mods)) System.out.print("private ");
            if (Modifier.isStatic(mods)) System.out.print("static ");

            field.setAccessible(true);
            System.out.println(field.getType().getCanonicalName() + ' ' + field.getName() + ' ' +field.get(instance));
        }
    }

    public static void main(String[] args) throws IllegalAccessException {
        Task2 task2Instance = new Task2();
        task2Instance.setI(999);
        printAllFieldsWithValues(task2Instance);

    }
}
