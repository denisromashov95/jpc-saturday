package week1.exceptions.task1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/*
    Необходимо корректно считать файл построчно и обработать исключение (использовать try-with-resources)
    А также вывести построчно в консоль зачитанный файл по шаблону "LINE: " + {строчка считанная из файла}
    Для хранения считанных строк использовать ArrayList
 */
public class FileReader {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(new File("input.txt"))) {
            ArrayList<String> lines = new ArrayList<>();

            while (scanner.hasNextLine()) {
                lines.add(scanner.nextLine());
            }

            for (String line : lines) {
                System.out.println("LINE: " + line);
            }
        } catch (FileNotFoundException e) {
            System.out.println("LOG: " + e.getMessage());
        }

    }
}
