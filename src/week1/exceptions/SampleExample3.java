package week1.exceptions;

public class SampleExample3 {
    public static void main(String[] args) throws MyMathException {
        int result = division(55, 0);
        System.out.println(result);
    }

    public static int division(int a, int b) throws MyMathException {
        try {
            return a / b;
//            throw new MyMathException("Произошло деление на 0.");
        } catch (ArithmeticException e) {
            throw new RuntimeException(e);
        } finally {
            throw new IllegalArgumentException("from finally!");
        }
    }
}
