package week1.exceptions;

public class MultipleExceptions {
    public static void main(String[] args) {
        try {
            someMethodThrowArrayIndexOutOfBoundsException();
            toDivideThrowMyMathException(100, 1);
            simpleThrowRuntimeException();

            //1 вариант - общий родитель (Проблема одинаковая обработка (одинаковый текст ошибки))
            // instance of - как запасной вариант решения проблемы

            //2 вариант - множественная запись исключений (catch (E1 | E2 ...))

            //3 вариант - несколько catch блоков
        } catch (MyMathException e1) {
            System.out.println("LOG: Деление на 0.");
        } catch (ArrayIndexOutOfBoundsException e2) {
            System.out.println("LOG: Проблемы с массивом.");
        } catch (RuntimeException e3) {
            System.out.println("LOG: Рантайм исключение.");
        }

    }

    public static void toDivideThrowMyMathException(int a, int b) throws MyMathException {
        try {
            System.out.println(a / b);
        } catch (ArithmeticException e) {
            throw new MyMathException(e.getMessage());
        }
    }

    public static void someMethodThrowArrayIndexOutOfBoundsException() {
        int[] arr = new int[10];
        System.out.println(arr[9]);
    }

    public static void simpleThrowRuntimeException() {
        throw new RuntimeException();
    }

}
