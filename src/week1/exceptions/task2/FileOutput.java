package week1.exceptions.task2;

/*
Написать Hello в output.txt (использовать writer с ресурсами)
и обработать исключение в catch вывести в консоль "LOG: " + {сообщение исключения}
 */

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class FileOutput {
    private static final String PKG_DIRECTORY = "/Users/denisromashov/Sber/Repositories/JPC/jpc-saturday/src/week1.exceptions/task2";
    private static final String OUTPUT_FILE_NAME = "output.txt";
    public static void main(String[] args) {
        try (Writer wr = new FileWriter(PKG_DIRECTORY + "/" + OUTPUT_FILE_NAME)) {
            wr.write("Hello!");
        } catch (IOException e) {
            System.out.println("LOG: " + e.getMessage());
        }
    }
}
