package week1.exceptions;

import java.io.IOException;

public class SampleExample {
    public static void main(String[] args) throws IOException {
       int result = division(55,0);
        System.out.println(result);
    }

    public static int division(int a, int b) throws IOException, IllegalArgumentException {
        if (b == 0) {
            throw new IOException("The divisor cannot be zero!");
        }
       return a / b;
    }
}
