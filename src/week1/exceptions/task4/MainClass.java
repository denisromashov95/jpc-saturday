package week1.exceptions.task4;

import week1.exceptions.task4.customexception.MyBaseException;

public class MainClass {
    public static void main(String[] args) {
        Calc calculator = new Calc();

        try {
            calculator.input();
            System.out.println(calculator.calculate());
        } catch (MyBaseException e) {
            System.out.println("Произошла неизвестная ошибка!");
        }
    }
}
