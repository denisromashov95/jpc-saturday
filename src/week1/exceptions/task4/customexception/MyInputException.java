package week1.exceptions.task4.customexception;

public class MyInputException extends MyBaseException {

    public MyInputException(String message) {
        super(message);
    }

    public MyInputException() {
        super("Неправильный формат ввода");
    }
}
