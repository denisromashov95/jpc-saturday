package week1.exceptions.task4.customexception;

public class MyBaseException extends RuntimeException {

    public MyBaseException(String message) {
        super(message);
        System.out.println("LOG: " + message);
    }
}
