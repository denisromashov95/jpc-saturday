package week1.exceptions.task4.customexception;

public class MyDivisionByZeroException extends MyBaseException {

    public MyDivisionByZeroException(String message) {
        super(message);
    }

    public MyDivisionByZeroException() {
        super("Недопустимое деление на 0.");
    }
}
