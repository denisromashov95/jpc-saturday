package week1.exceptions.task4.customexception;

public class MyWrongOperatorException extends  MyBaseException {
    public MyWrongOperatorException(String message) {
        super(message);
    }

    public MyWrongOperatorException() {
        super("Недопустимый оператор для калькулятора.");
    }
}
