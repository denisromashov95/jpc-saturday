package week1.exceptions.task4.customexception;

public class MyIntOverflowException extends MyBaseException {
    public MyIntOverflowException(String message) {
        super(message);
    }

    public MyIntOverflowException() {
        super("Выход за пределы допустимых значений int.");
    }
}
