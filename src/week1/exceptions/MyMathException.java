package week1.exceptions;

public class MyMathException extends Exception {
//        extends ArithmeticException { - unchecked


    public MyMathException() {
        //Можно по дефолту передавать сообщение об ошибки, если оно одно и тоже
        //super("Произошло деление на 0.");
    }

    public MyMathException(String message) {
        super(message);
    }

    public MyMathException(String message, Throwable cause) {
        super(message, cause);
    }
}
