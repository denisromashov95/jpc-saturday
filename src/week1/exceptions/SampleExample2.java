package week1.exceptions;

public class SampleExample2 {
    public static void main(String[] args) {
        try {
            int result = division(55,0);
            System.out.println(result);
        } catch (MyMathException exception) {
            System.out.println(exception.getMessage());
        }
    }

    public static int division(int a, int b) throws MyMathException {
        try {
            return a / b;
        } catch (ArithmeticException e) {
            System.err.println("ArithmeticException -> Catch!");
            throw new MyMathException("Произошло деление на 0.", e);
        } finally {
            System.err.println("Hello from finally!");
        }
    }
}
