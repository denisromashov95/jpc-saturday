package week1.exceptions.resources;

public class MyResource implements AutoCloseable {
    @Override
    public void close() throws Exception {
        System.out.println("Closed MyResource!");
    }

    public void printHello() {
        System.out.println("Hello!");
    }
}
