package week6.consultation.hw1;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.Date;

public class DateExample {


    /*
    b. public void checkBirthdate(String str) — дата рождения должна быть не
        раньше 01.01.1900 и не позже текущей даты.
     */
    public static void main(String[] args) throws ParseException {
        String string = "30.02.2023";
        checkBirthdate(string);

        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = dateFormat.parse(string);
        System.out.println(date);

        LocalDate localDate = LocalDate.parse(string, DateTimeFormatter.ofPattern("dd.MM.uuuu").withResolverStyle(ResolverStyle.STRICT));
        System.out.println(localDate);

    }


    public static void checkBirthdate(String str) throws IllegalArgumentException{
        Date date = new Date(Date.parse(str));
    }
}
