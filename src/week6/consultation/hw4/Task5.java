package week6.consultation.hw4;


import java.util.List;
import java.util.stream.Collectors;

/*
        На вход подается список непустых строк. Необходимо привести все символы строк к
        верхнему регистру и вывести их, разделяя запятой.
        Например, для List.of("abc", "def", "qqq") результат будет ABC, DEF, QQQ.
 */
public class Task5 {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "def", "qqq");

        System.out.println(list.stream()
                .map(String::toUpperCase)
                .collect(Collectors.joining(", ")));

        String str = String.join(", ", list.stream().map(String::toUpperCase).toList());
        System.out.println(str);
    }
}
