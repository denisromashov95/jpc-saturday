-- Ирина, подруга Пети, решила создать свой бизнес по продаже цветов. Начать
-- она решила с самых основ: создать соответствующую базу данных для своего
-- бизнеса. Она точно знает, что будет продавать Розы по 100 золотых монет за
-- единицу, Лилии по 50 и Ромашки по 25.

create table if not exists flowers
(
    id      serial primary key,
    title   varchar(100) UNIQUE check (title <> '') not null,
    price   integer not null
);

insert into flowers (title, price)
values ('Розы', 100);
insert into flowers (title, price)
values ('Лилии', 50);

insert into flowers
values (nextval('flowers_id_seq'), 'Ромашки', 25);

select * from flowers;

insert into flowers (title, price)
values ('Розы', 150);

insert into flowers (title, price)
values ('Розы Краснодарские', 150);

-- Помимо этого, ей хочется хранить данные своих покупателей (естественно они
-- дали согласие на хранение персональной информации). Сохранять нужно Имя
-- и Номер телефона.

create table if not exists clients
(
    id      serial primary key,
    name    varchar(50) not null,
    phone   varchar(30) CHECK (phone ~ '\+7 \(\d{3}\) \d{3}\-\d{2}\-\d{2}') not null
);

insert into clients (name, phone)
values ('Анна', '+7 (960) 123-45-67');

select * from clients;

insert into clients (name, phone)
values ('Вениамин', '+7 (900) 123-45-67'),
       ('Инга', '+7 (951) 000-11-22'),
       ('Николай', '+7 (960) 555-20-30'),
       ('Петр', '+7 (908) 505-21-39');

-- И, конечно, данные самого заказа тоже нужно как-то хранить! Ирина пока не
-- продумала поля, но она точно хочет следовать следующим правилам:
-- ● в рамках одного заказа будет продавать только один вид цветов
-- (например, только розы)
-- ● в рамках одного заказа можно купить от 1 до 1000 единиц цветов.

create table if not exists orders
(
    id          serial primary key,
    client_id   integer references clients (id)                   not null,
    flower_id    integer references flowers (id)                    not null,
    amount      integer check (amount >= 1 and amount <= 1000)    not null,
    date        timestamp                                         not null
);


insert into orders (client_id, flower_id, amount, date)
values (2, 2, 10, '2022-09-01'),
       (2, 2, 5, '2022-09-10'),
       (3, 1, 3, '2022-09-30'),
       (3, 1, 6, '2022-10-02'),
       (4, 5, 6, '2023-02-02'),
       (5, 3, 6, '2022-03-02'),
       (4, 2, 500, '2022-04-06'),
       (5, 5, 101, '2022-04-06');

select * from flowers;
select * from clients;
select * from orders;

-- 1. По идентификатору заказа получить данные заказа и данные клиента,
-- создавшего этот заказ

select o.id as order_id,
       f.title as type_of_flowers,
       o.amount,
       c.name,
       c.phone
from orders o
    inner join clients c on c.id = o.client_id
    inner join flowers f on f.id = o.flower_id
where o.id = ?;

-- 4. Вывести общую выручку (сумму золотых монет по всем заказам) за все
-- время
select sum(o.amount * f.price) as total
from orders o
    inner join flowers f on f.id = o.flower_id;

-- 3. Найти заказ с максимальным количеством купленных цветов, вывести их
-- название и количество
select o.id, o.date, f.title, o.amount
from orders o
    inner join flowers f on f.id = o.flower_id
order by amount desc
limit 1;